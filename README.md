#creating Golden Image (AMI) using packer with CloudWatch Agent,AWS SSM Agent,Docker installed in it.

#Applications/Tools Used
1.PACKER
2.GIT
3.SHELL SCRIPT
4.AWS
5.JENKINS

#Pre-requisites
1.AWS Account (preferably with IAM Role).
2.A Jenkins Instance
3.GitHub Account
4.Linux environment for Shell Scripting.
5.Packer installation


#Download packer for your windows/Mac/Linux system Click


# Initiate 
packer init .

# Validate
packer validate .

# Build
packer build .

